package id.sisi.forca.report.component;

import id.sisi.forca.report.print.ReportStarterBook;

import org.adempiere.base.IProcessFactory;
import org.adempiere.util.ProcessUtil;
import org.compiere.process.ProcessCall;

public class ReportProcessPrintFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {
		ProcessCall process = null;
		Class<?> processClass = null;
		if (className
				.equals("id.sisi.forca.report.print.ReportStarterBook")) {
			processClass = ReportStarterBook.class;
		} 
		if (processClass != null) {
			try {
				process = (ProcessCall) processClass.newInstance();
				return process;
			} catch (Exception ex) {
				return null;
			}
		}
		return null;
	}

}
